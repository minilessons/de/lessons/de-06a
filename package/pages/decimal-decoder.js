function DecimalDecoder(canvas, context, input, inputNames, output){
    this.canvas = canvas;
    this.context = context;
    this.input = input;
    this.inputNames = inputNames;
    this.output = output;
    this.inputPoints = [];
    this.outputPoints = [];
    this.initialized = false;

    this.getInput = function(){
        return this.input;
    }

    this.getInputPoints = function(){
        return this.inputPoints;
    }

    this.getOutputPoints = function(){
        return this.outputPoints;
    }

    this.width = 80;
    this.height = 120;
    this.getWidth = function(){
        return this.width;
    }

    this.getHeight = function(){
        return this.height;
    }

    this.setWidth = function(width){
        this.width = width;
    }
    this.setHeight = function(height){
        this.height = height;
    }

    this.fontSize = 14;
    this.setFontSize = function(fontSize){
        this.fontSize = fontSize;
    }

    var inputCount = input.elems.length;

    this.topLeft = new Point(canvas.width/2 - this.width/2, (canvas.height - this.height) / 2);
    this.topRight = new Point(this.topLeft.x + this.width, this.topLeft.y);
    this.downLeft = new Point(this.topLeft.x, this.topLeft.y + this.height);
    this.downRight = new Point(this.topLeft.x + this.width, this.topLeft.y + this.height);

    this.setTopLeft = function(topLeft){
        this.topLeft = topLeft;
        this.topRight = new Point(this.topLeft.x + this.width, this.topLeft.y);
        this.downLeft = new Point(this.topLeft.x, this.topLeft.y + this.height);
        this.downRight = new Point(this.topLeft.x + this.width, this.topLeft.y + this.height);
    }
    
    var inputLength = 10;
    var inputSize = 20;

    this.drawAll = function(){
        // clear
        context.clearRect(0, 0, canvas.width, canvas.height); 
        
        this.draw();
        this.drawOutput();
        this.drawInput();
    }

    this.drawInput = function(){
        var spacing = this.height / (inputCount + 1);
        input.positions = [];
        for (var i = 0; i < inputCount; ++i){
            var start = new Point(this.downLeft.x, this.downLeft.y - (i + 1)*spacing);
            context.beginPath();
            context.font = this.fontSize + "px Arial";
            context.fillText(input.elems[i], start.x - inputLength - inputSize + 3, start.y + inputSize/2 - 5);        
            context.stroke();

            context.beginPath();
            context.setLineDash([2, 2]);
            context.rect(start.x - inputLength - inputSize - 3, start.y - inputSize/2, inputSize, inputSize);
            input.positions[i] = {x: start.x - inputLength - inputSize - 3, y: start.y - inputSize/2, height: inputSize};
            context.stroke();
            context.setLineDash([]);
        }

        if (this.initialized) return;
        this.initialized = true;
        var decoder = this;
        canvas.addEventListener('mousedown', function(evt) {
            var rect = canvas.getBoundingClientRect();
            var pos = new Point(evt.clientX - rect.left, evt.clientY - rect.top);
            
            for(var i = 0; i < input.positions.length; i++) {
                var p = input.positions[i];
                if(pos.x>=p.x && pos.x<p.x+p.height && pos.y>=p.y && pos.y<p.y+p.height) {
                    input.elems[i] = 1 - input.elems[i];
                    decoder.drawAll();
                    break;
                }
            }
        }, false);
    }

    this.drawOutput = function(){
        var outputs = Math.pow(2, inputCount);
        var output = getOutput(input.elems);
        var digit = 0;
        if (this.isEnabled == 0){
            output = -1;
        }
        var spacing = this.height / (10 + 1);
        context.beginPath();
        for (var i = 0; i < outputs; ++i){
            if (this.output[i] == '?'){
                continue;
            }
            var start = new Point(this.topRight.x, this.topRight.y + (digit + 1)*spacing);
            context.fillText(i == output? "1" : "0", start.x + inputLength + 5, start.y + 5);
            digit++;
        }
        context.stroke();
    }

    this.draw = function() {
        //drawing the outline of the decoder
        context.beginPath();
        context.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
        context.stroke();

        //drawing the input lines
        var spacing = this.height / (inputCount + 1);
        context.beginPath();
        this.inputPoints = [];
        for (var i = 0; i < inputCount; ++i){
            var start = new Point(this.downLeft.x, this.downLeft.y - (i + 1)*spacing);
            context.moveTo(start.x, start.y);
            context.lineTo(start.x - inputLength, start.y);
            this.inputPoints.push(new Point(start.x - inputLength, start.y));

            context.font = this.fontSize + "px Arial";
            var text = this.inputNames[i];
            context.fillText(text, start.x + 5, start.y + 5);           
        }
        context.stroke();

        //drawing the output lines
        var outputs = 10;
        spacing = this.height / (outputs + 1);
        context.beginPath();
        this.outputPoints = [];
        for (var i = 0; i < outputs; ++i){
            var start = new Point(this.topRight.x, this.topRight.y + (i + 1)*spacing);
            context.moveTo(start.x, start.y);
            context.lineTo(start.x + inputLength, start.y);
            this.outputPoints.push(start.x + inputLength, start.y);

            context.font = this.fontSize + "px Arial";
            var text = i;
            var textWidth = context.measureText(text).width;
            context.fillText(text, start.x - 5 - textWidth, start.y + 5);
        }
        context.stroke();   
    }
}

function setupDecimalDecoder(canvasName, initialInput, outputs) {
  var canvas = document.getElementById(canvasName);
  var context = canvas.getContext("2d");
  var input = {elems: initialInput, positions: []};
  var inputNames = ['A0', 'A1', 'A2', 'A3'];
  var output = outputs;

  var decoder = new DecimalDecoder(canvas, context, input, inputNames, output);
  decoder.setFontSize(12);

  return {canvas: canvas, context: context, decoder: decoder};
}

function setupBCDDecoder(canvasName) {
  return setupDecimalDecoder(canvasName, [0,0,0,0], ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '?', '?', '?', '?', '?', '?']);
}

function setupEx3Decoder(canvasName) {
  return setupDecimalDecoder(canvasName, [1,1,0,0], ['?', '?', '?' ,'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '?', '?', '?']);
}

function setupAikenDecoder(canvasName) {
  return setupDecimalDecoder(canvasName, [0,0,0,0], ['0', '1', '2', '3', '4', '?', '?', '?', '?', '?', '?', '5', '6', '7', '8', '9']);
}

