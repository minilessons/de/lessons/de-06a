function PriorityExcess3Coder() {

  var n = 4;
  PriorityCoder.call(this, 10, 4);

  this.calculateOutput = function(i) {
    for (var j = 0; j < n; ++j) {
      this.output[n-j-1] = (i+3 >> j) & 1;
    }
  }

  this.activateInput(0);
}

PriorityExcess3Coder.prototype = Object.create(PriorityCoder.prototype);
