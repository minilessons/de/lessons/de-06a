var BasicCoder = function(m, n) {

  this.input = new Array(m).fill(0);
  this.output = new Array(n).fill(0);
  this.inputPositions = [];

  this.activateInput = function(i) {
      this.resetInput();
      this.input[i] = 1;
      this.calculateOutput(i);
  };

  this.resetInput = function() {
      this.input.fill(0);
  };

  this.draw = function (canvas, context) {
    var width = 80;
    var height = 400;

    this.drawBasic(width, height, canvas, context);
  }

  this.drawBasic = function (width, height, canvas, context) {
        context.clearRect(0, 0, canvas.width, canvas.height);

        var topLeft = new Point(canvas.width/2 - width/2, (canvas.height - height) / 2);
        var topRight = new Point(canvas.width/2 + width/2, (canvas.height - height) / 2);
        var downLeft = new Point(canvas.width/2 - width/2, canvas.height - (canvas.height - height) / 2);
        var downRight = new Point(canvas.width/2 + width/2, canvas.height - (canvas.height - height) / 2);

        // decoder
        context.beginPath();
        context.rect(topLeft.x, topLeft.y, width, height);
        context.stroke();

        // input
        var length = 10;
        var inputSize = 20;
        var spacing = height / (this.input.length + 1);
        for (var i = 0; i < this.input.length; ++i){
            var start = new Point(downLeft.x, downLeft.y - (i + 1)*spacing);
            context.beginPath();
            context.moveTo(start.x, start.y);
            context.lineTo(start.x - length - 3, start.y);

            context.font = "14px Arial";
            context.fillText("I" + i, start.x + 5, start.y + 5);

            context.fillStyle = "gray";
            context.fillRect(start.x - length - inputSize - 3, start.y - inputSize/2, inputSize , inputSize);
            context.fillStyle = "black";

            context.fillText(this.input[i], start.x - length - inputSize + 3, start.y + inputSize/2 - 5);

            context.stroke();

            context.beginPath();
            context.rect(start.x - length - inputSize - 3, start.y - inputSize/2, inputSize, inputSize);
            this.inputPositions[i] = {x: start.x - length - inputSize - 3, y: start.y - inputSize/2, height: inputSize};
            context.stroke();
        }


        // output
        // spacing = height / (this.output.length + 1);
        context.beginPath();
        for (var i = 0; i < this.output.length; ++i){
            var start = new Point(downRight.x, downRight.y - ((this.input.length-this.output.length)/2+1+i)*spacing);
            context.moveTo(start.x, start.y);
            context.lineTo(start.x + length, start.y);

            context.font = "14px Arial";
            var text = i;
            var textWidth = context.measureText(text).width;
            context.fillText("A" + text, start.x - 15 - textWidth, start.y + 5);

            context.fillText(this.output[this.output.length - i - 1], start.x + length + 5, start.y + 5);
        }
        context.stroke();
    };

}

BasicCoder.prototype.calculateOutput = function(i) { }
