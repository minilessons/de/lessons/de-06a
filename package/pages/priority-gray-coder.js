function PriorityGrayCoder(n) {

  PriorityCoder.call(this, 1<<n, n);

  this.calculateOutput = function(i) {
    for (var j = 0; j < n - 1; ++j) {
      this.output[n-j-1] = ((i >> j) ^ (i >> j+1)) & 1;
    }
    this.output[0] = (i >> n-1) & 1;
  }

  this.activateInput(0);
}

PriorityGrayCoder.prototype = Object.create(PriorityCoder.prototype);
