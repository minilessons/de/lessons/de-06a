//---------------------- Helper methods ----------------------
function Point(x, y){
    this.x = x;
    this.y = y;
}

function getOutput(input, n){
    var sol = 0;
    var powerOfTwo = 1;
    for (var i = 0; i < input.length; ++i){
        if (input[i] == 1){
            sol += powerOfTwo;
        }

        powerOfTwo *= 2;
    }

    return sol;
}
