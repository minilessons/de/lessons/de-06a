function Decoder(canvas, context, input, hasEnabling){
    this.canvas = canvas;
    this.context = context;
    this.input = input;
    this.hasEnabling = hasEnabling;
    this.inputPoints = [];
    this.outputPoints = [];
    this.isEnabled = 1;
    this.initialized = false;

    this.getInput = function(){
        return this.input;
    }

    this.getInputPoints = function(){
        return this.inputPoints;
    }

    this.getOutputPoints = function(){
        return this.outputPoints;
    }

    this.setEnabled = function(enabled){
        this.isEnabled = enabled;
    }

    this.width = 80;
    this.height = 120;
    this.getWidth = function(){
        return this.width;
    }

    this.getHeight = function(){
        return this.height;
    }

    this.setWidth = function(width){
        this.width = width;
    }
    this.setHeight = function(height){
        this.height = height;
    }

    var inputCount = input.elems.length;

    if (hasEnabling) {
        this.topLeft = new Point(canvas.width/2 - this.width/2, (canvas.height - this.height) / 2 + 10);
    } else {
        this.topLeft = new Point(canvas.width/2 - this.width/2, (canvas.height - this.height) / 2);
    }

    this.topRight = new Point(this.topLeft.x + this.width, this.topLeft.y);
    this.downLeft = new Point(this.topLeft.x, this.topLeft.y + this.height);
    this.downRight = new Point(this.topLeft.x + this.width, this.topLeft.y + this.height);

    this.setTopLeft = function(topLeft){
        this.topLeft = topLeft;
        this.topRight = new Point(this.topLeft.x + this.width, this.topLeft.y);
        this.downLeft = new Point(this.topLeft.x, this.topLeft.y + this.height);
        this.downRight = new Point(this.topLeft.x + this.width, this.topLeft.y + this.height);
    }
    
    var inputLength = 10;
    var inputSize = 20;

    this.drawAll = function(){
        // clear
        context.clearRect(0, 0, canvas.width, canvas.height); 
        
        this.draw();
        this.drawEnabling();
        this.drawOutput();
        this.drawInput();
    }

    this.drawInput = function(){
        var spacing = this.height / (inputCount + 1);
        input.positions = [];
        for (var i = 0; i < inputCount; ++i){
            var start = new Point(this.downLeft.x, this.downLeft.y - (i + 1)*spacing);
            context.beginPath();
            context.font = "14px Arial";
            context.fillText(input.elems[i], start.x - inputLength - inputSize + 3, start.y + inputSize/2 - 5);        
            context.stroke();

            context.beginPath();
            context.setLineDash([2, 2]);
            context.rect(start.x - inputLength - inputSize - 3, start.y - inputSize/2, inputSize, inputSize);
            input.positions[i] = {x: start.x - inputLength - inputSize - 3, y: start.y - inputSize/2, height: inputSize};
            context.stroke();
            context.setLineDash([]);
        }

        if (this.initialized) return;
        this.initialized = true;
        var decoder = this;
        canvas.addEventListener('mousedown', function(evt) {
            var rect = canvas.getBoundingClientRect();
            var pos = new Point(evt.clientX - rect.left, evt.clientY - rect.top);
            
            for(var i = 0; i < input.positions.length; i++) {
                var p = input.positions[i];
                if(pos.x>=p.x && pos.x<p.x+p.height && pos.y>=p.y && pos.y<p.y+p.height) {
                    input.elems[i] = 1 - input.elems[i];
                    decoder.drawAll();
                    break;
                }
            }
        }, false);
    }

    this.drawOutput = function(){
        var outputs = Math.pow(2, inputCount);
        var output = getOutput(input.elems);
        if (this.isEnabled == 0){
            output = -1;
        }
        var spacing = this.height / (outputs + 1);
        context.beginPath();
        for (var i = 0; i < outputs; ++i){
            var start = new Point(this.downRight.x, this.downRight.y - (i + 1)*spacing);
            context.fillText(i == output? "1" : "0", start.x + inputLength + 5, start.y + 5);
        }
        context.stroke();
    }

    var enableBox = {x: 0, y:0, height:0};
    this.drawEnabling = function(){
        if (!hasEnabling) return;

        context.beginPath();
        var start = new Point((this.topLeft.x + this.topRight.x) / 2, this.topLeft.y);
        start.y -= inputLength;
        context.moveTo(start.x, start.y);

        var length = inputLength + start.x - this.topLeft.x; 
        context.lineTo(start.x - length, start.y);

        start.x = start.x - length;
        context.font = "14px Arial";
        context.fillText(this.isEnabled, start.x - inputSize + 3, start.y + inputSize/2 - 5);        
        context.stroke();

        context.beginPath();
        context.setLineDash([2, 2]);
        context.rect(start.x - inputSize - 3, start.y - inputSize/2, inputSize, inputSize);
        enableBox = {x: start.x - inputSize - 3, y: start.y - inputSize/2, height: inputSize};
        context.stroke();
        context.setLineDash([]);

        if (this.initialized) return;
        var decoder = this;
        canvas.addEventListener('mousedown', function(evt) {
            var rect = canvas.getBoundingClientRect();
            var pos = new Point(evt.clientX - rect.left, evt.clientY - rect.top);
            
            var p = enableBox;
            if(pos.x>=p.x && pos.x<p.x+p.height && pos.y>=p.y && pos.y<p.y+p.height) {
                decoder.isEnabled = 1 - decoder.isEnabled;
                decoder.drawAll();
            }
        }, false);
    }

    this.draw = function() {
        //drawing the outline of the decoder
        context.beginPath();
        context.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
        context.stroke();

        //drawing the input lines
        var spacing = this.height / (inputCount + 1);
        context.beginPath();
        this.inputPoints = [];
        for (var i = 0; i < inputCount; ++i){
            var start = new Point(this.downLeft.x, this.downLeft.y - (i + 1)*spacing);
            context.moveTo(start.x, start.y);
            context.lineTo(start.x - inputLength, start.y);
            this.inputPoints.push(new Point(start.x - inputLength, start.y));

            context.font = "14px Arial";
            context.fillText("A" + i, start.x + 5, start.y + 5);           
        }
        context.stroke();

        //drawing the output lines
        var outputs = Math.pow(2, inputCount);
        spacing = this.height / (outputs + 1);
        context.beginPath();
        this.outputPoints = [];
        for (var i = 0; i < outputs; ++i){
            var start = new Point(this.downRight.x, this.downRight.y - (i + 1)*spacing);
            context.moveTo(start.x, start.y);
            context.lineTo(start.x + inputLength, start.y);
            this.outputPoints.push(start.x + inputLength, start.y);

            context.font = "14px Arial";
            var text = "I"+i;
            var textWidth = context.measureText(text).width;
            context.fillText(text, start.x - 5 - textWidth, start.y + 5);
        }
        context.stroke();   

        //drawing the enable line
        if (hasEnabling){
            context.beginPath();
            var start = new Point((this.topLeft.x + this.topRight.x) / 2, this.topLeft.y);
            context.moveTo(start.x, start.y);
            context.lineTo(start.x, start.y - inputLength);

            context.font = "14px Arial";
            context.fillText("E", start.x - 5, start.y + 17);
            context.stroke();
        }
    }
}

function setupDecoder(canvasName, inputs, hasEnable) {
  var canvas = document.getElementById(canvasName);
  var context = canvas.getContext("2d");
  var input = {elems: inputs, positions: []};
  var decoder = new Decoder(canvas, context, input, hasEnable);
  return {canvas: canvas, context: context, decoder: decoder};
}

