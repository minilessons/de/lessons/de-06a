function BCDCoder() {

  var n = 4;
  BasicCoder.call(this, 10, 4);

  this.calculateOutput = function(i) {
    for (var j = 0; j < n; ++j) {
      this.output[n-j-1] = (i >> j) & 1;
    }
  }

  this.activateInput(0);
}

BCDCoder.prototype = Object.create(BasicCoder.prototype);
